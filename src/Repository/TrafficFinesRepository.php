<?php

namespace App\Repository;

use App\Entity\TrafficFines;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrafficFines|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrafficFines|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrafficFines[]    findAll()
 * @method TrafficFines[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrafficFinesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrafficFines::class);
    }

    // /**
    //  * @return TrafficFines[] Returns an array of TrafficFines objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TrafficFines
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
