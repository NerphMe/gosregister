<?php

namespace App\Repository;

use App\Entity\TheProperty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TheProperty|null find($id, $lockMode = null, $lockVersion = null)
 * @method TheProperty|null findOneBy(array $criteria, array $orderBy = null)
 * @method TheProperty[]    findAll()
 * @method TheProperty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThePropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TheProperty::class);
    }

    // /**
    //  * @return TheProperty[] Returns an array of TheProperty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TheProperty
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
