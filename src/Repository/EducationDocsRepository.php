<?php

namespace App\Repository;

use App\Entity\EducationDocs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EducationDocs|null find($id, $lockMode = null, $lockVersion = null)
 * @method EducationDocs|null findOneBy(array $criteria, array $orderBy = null)
 * @method EducationDocs[]    findAll()
 * @method EducationDocs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EducationDocsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EducationDocs::class);
    }

    // /**
    //  * @return EducationDocs[] Returns an array of EducationDocs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EducationDocs
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
