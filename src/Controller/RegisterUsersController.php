<?php

namespace App\Controller;

use App\Entity\Cars;
use App\Entity\Citizen;
use App\Entity\Documents;
use App\Entity\DriverLicense;
use App\Entity\EducationDocs;
use App\Entity\TheProperty;
use App\Entity\TrafficFines;
use App\Form\CarsType;
use App\Form\CitizenType;
use App\Form\DriverLicenseType;
use App\Form\EducationsDocType;
use App\Form\PassportType;
use App\Form\PropertyType;
use App\Form\TrafficFinesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RegisterUsersController extends AbstractController
{


    /**
     * @Route("/addCars", name="addCars")
     * @param Request $request
     * @return Response
     */
    public function addCars(Request $request): Response
    {
        $cars = new Cars();
        $form = $this->createForm(CarsType::class, $cars);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cars = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($cars);
            $em->flush();
        }

        return $this->render('register_users/addCars.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/addCitizen", name="addCitizen")
     * @param Request $request
     * @return Response
     */
    public function addCitizens(Request $request): Response
    {
        $citizen = new Citizen();
        $form = $this->createForm(CitizenType::class, $citizen);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $citizen = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($citizen);
            $em->flush();
        }


        return $this->render('register_users/addCitizens.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/addPassport", name="addPasport")
     * @param Request $request
     * @return Response
     */
    public function addPassport(Request $request): Response
    {
        $passport = new Documents();
        $form = $this->createForm(PassportType::class, $passport);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $passport = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($passport);
            $em->flush();
        }


        return $this->render('register_users/addPassport.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/addEducationDocs", name="addEducation")
     * @param Request $request
     * @return Response
     */
    public function addEducation(Request $request): Response
    {
        $docs = new EducationDocs();
        $form = $this->createForm(EducationsDocType::class, $docs);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $docs = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($docs);
            $em->flush();
        }


        return $this->render('register_users/addEducations.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/addDriverLicence", name="addLicense")
     * @param Request $request
     * @return Response
     */
    public function addDriverLicense(Request $request): ?Response
    {
        $dl = new DriverLicense();
        $form = $this->createForm(DriverLicenseType::class, $dl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dl = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($dl);
            $em->flush();
        }


        return $this->render('register_users/addDriverLicense.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/addTrafficeFines", name="addFines")
     * @param Request $request
     * @return Response
     */
    public function addTrafficFines(Request $request): ?Response
    {
        $tf = new TrafficFines();
        $form = $this->createForm(TrafficFinesType::class, $tf);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tf = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($tf);
            $em->flush();
        }


        return $this->render('register_users/addDriverLicense.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/addProperty", name="addProperty")
     * @param Request $request
     * @return Response
     */
    public function addProperty(Request $request): Response
    {
        $property = new TheProperty();
        $form = $this->createForm(PropertyType::class, $property);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $property = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($property);
            $em->flush();
        }


        return $this->render('register_users/addProperty.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/allCitizenInfo", name="allInfo")
     */
    public function getAllCitizens(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $citizen = $em->getRepository(Citizen::class)->findAll();

        return $this->render('register_users/All.html.twig', [
            'citizens' => $citizen]);

    }

    /**
     * @Route("/cars", name="cars")
     */
    public function carsTable(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $cars = $em->getRepository(Cars::class)->findAll();

        return $this->render('register_users/cars.html.twig', [
            'cars' => $cars]);

    }

    /**
     * @Route("/driverLicense", name="dL")
     */
    public function driverLicenseTable(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $driverLicense = $em->getRepository(DriverLicense::class)->findAll();

        return $this->render('register_users/driverLicense.html.twig', [
            'driversLicense' => $driverLicense]);

    }

    /**
     * @Route("/educatioDocs", name="docs")
     */
    public function educationDocsTable(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $eductaionDocs = $em->getRepository(EducationDocs::class)->findAll();

        return $this->render('register_users/educationDocsTable.html.twig', [
            'educationDoc' => $eductaionDocs]);

    }

    /**
     * @Route("/passportTable", name="passport")
     */
    public function passportTable(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $passport = $em->getRepository(Documents::class)->findAll();

        return $this->render('register_users/passportTable.html.twig', [
            'passports' => $passport]);

    }

    /**
     * @Route("/porpertyTable", name="property")
     */
    public function propertyTable(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository(TheProperty::class)->findAll();

        return $this->render('register_users/propertyTable.html.twig', [
            'propertys' => $property]);

    }

    /**
     * @Route("/Citizencars/{id}", name="carsCitizens")
     * @param $id
     * @return Response
     */
    public function citizenCars($id): Response
    {

        $citizenz = $this->getDoctrine()->getRepository(Cars::class)->find($id);

        return $this->render('register_users/cars.html.twig', [
            'citizens' => $citizenz]
        );
    }

}
