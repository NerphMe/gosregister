<?php

namespace App\Form;

use App\Entity\Citizen;
use App\Entity\EducationDocs;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EducationsDocType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('diplom')
            ->add('atestat')
            ->add('issuedBy')
            ->add('whenIssued')
            ->add('subjectRatings')
        ;
        $builder
            ->add('citizen', EntityType::class, [
                'class' => Citizen::class,
                'choice_label' => 'name'
            ])

            ->add('Save', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EducationDocs::class,
        ]);
    }
}
