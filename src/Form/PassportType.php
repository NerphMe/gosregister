<?php

namespace App\Form;

use App\Entity\Citizen;
use App\Entity\Documents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PassportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Passport')
            ->add('serialNumber')
            ->add('international_passport')
            ->add('issuedBy')
            ->add('whenIssued')
            ->add('untilWhenValid')
            ->add('citizen')

        ;
        $builder
            ->add('citizen', EntityType::class,[
                'class' => Citizen::class,
                'choice_label' => 'name'
                ])
            ->add('Save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Documents::class,
        ]);
    }
}
