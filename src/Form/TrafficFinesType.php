<?php

namespace App\Form;

use App\Entity\Cars;
use App\Entity\TrafficFines;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrafficFinesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fines')
            ->add('finesCost')
            ->add('statusOfFines')
        ;
        $builder
            ->add('cars', EntityType::class, [
                'class' => Cars::class,
                'choice_label' => 'carName'
            ])
            ->add('Save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrafficFines::class,
        ]);
    }
}
