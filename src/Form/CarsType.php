<?php

namespace App\Form;

use App\Entity\Cars;
use App\Entity\Citizen;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('carName')
            ->add('motorVolume')
            ->add('Transmission')
            ->add('carColor')
            ->add('vinCode');
        $builder
            ->add('citizen', EntityType::class,[
                'class' => Citizen::class,
                'choice_label' => 'name'
            ])

            ->add('Save', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cars::class,
        ]);
    }


}
