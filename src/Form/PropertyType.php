<?php

namespace App\Form;

use App\Entity\Citizen;
use App\Entity\TheProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeOfPorperty')
            ->add('area')
            ->add('adress')
        ;
        $builder
            ->add('citizen', EntityType::class,[
                'class' => Citizen::class,
                'choice_label' => 'name'
            ])

            ->add('Save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TheProperty::class,
        ]);
    }
}
