<?php

namespace App\Entity;

use App\Repository\EducationDocsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EducationDocsRepository::class)
 */
class EducationDocs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $diplom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $atestat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $issuedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $whenIssued;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subjectRatings;


    /**
     * @ORM\ManyToOne(targetEntity="Citizen", inversedBy="educationDocs")
     */
    private $citizen;


    /**
     * @return mixed
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

    /**
     * @param mixed $citizen
     */
    public function setCitizen($citizen): void
    {
        $this->citizen = $citizen;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiplom(): ?string
    {
        return $this->diplom;
    }

    public function setDiplom(?string $diplom): self
    {
        $this->diplom = $diplom;

        return $this;
    }

    public function getAtestat(): ?string
    {
        return $this->atestat;
    }

    public function setAtestat(?string $atestat): self
    {
        $this->atestat = $atestat;

        return $this;
    }

    public function getIssuedBy(): ?string
    {
        return $this->issuedBy;
    }

    public function setIssuedBy(?string $issuedBy): self
    {
        $this->issuedBy = $issuedBy;

        return $this;
    }

    public function getWhenIssued(): ?string
    {
        return $this->whenIssued;
    }

    public function setWhenIssued(?string $whenIssued): self
    {
        $this->whenIssued = $whenIssued;

        return $this;
    }

    public function getSubjectRatings(): ?string
    {
        return $this->subjectRatings;
    }

    public function setSubjectRatings(?string $subjectRatings): self
    {
        $this->subjectRatings = $subjectRatings;

        return $this;
    }
}
