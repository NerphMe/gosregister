<?php

namespace App\Entity;

use App\Repository\CarsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarsRepository", repositoryClass=CarsRepository::class)
 */
class Cars
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carName;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $motorVolume;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $Transmission;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $carColor;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $vinCode;

    /**
     * @ORM\ManyToOne(targetEntity="Citizen", inversedBy="cars")
     */
    private $citizen;

    /**
     * @ORM\OneToMany(targetEntity="TrafficFines", mappedBy="cars")
     */
    private $trafficFines;

    public function __construct()
    {
        $this->trafficFines = new ArrayCollection();
    }


    public function getTrafficFines(): ArrayCollection
    {
        return $this->trafficFines;
    }

    /**
     * @param ArrayCollection $trafficFines
     */
    public function setTrafficFines(ArrayCollection $trafficFines): void
    {
        $this->trafficFines = $trafficFines;
    }


    /**
     * @return mixed
     */
    public function getCitizen(): ?citizen
    {
        return $this->citizen;
    }

    /**
     * @param citizen $citizen
     * @return self
     */
    public function setCitizen(?citizen $citizen): self
    {
        $this->citizen = $citizen;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTransmission()
    {
        return $this->Transmission;
    }

    /**
     * @param mixed $Transmission
     */
    public function setTransmission($Transmission): void
    {
        $this->Transmission = $Transmission;
    }

    /**
     * @return mixed
     */
    public function getCarColor()
    {
        return $this->carColor;
    }

    /**
     * @param mixed $carColor
     */
    public function setCarColor($carColor): void
    {
        $this->carColor = $carColor;
    }

    /**
     * @return mixed
     */
    public function getVinCode()
    {
        return $this->vinCode;
    }

    /**
     * @param mixed $vinCode
     */
    public function setVinCode($vinCode): void
    {
        $this->vinCode = $vinCode;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarName()
    {
        return $this->carName;
    }

    public function setCarName($carName)
    {
        $this->carName = $carName;

    }

    public function getMotorVolume(): ?float
    {
        return $this->motorVolume;
    }

    public function setMotorVolume(?float $motorVolume): self
    {
        $this->motorVolume = $motorVolume;

        return $this;
    }
}
