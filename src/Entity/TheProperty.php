<?php

namespace App\Entity;

use App\Repository\ThePropertyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ThePropertyRepository::class)
 */
class TheProperty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeOfPorperty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $area;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;

    /**
     * @ORM\ManyToOne(targetEntity="Citizen", inversedBy="theProperty")
     */
    private $citizen;


    /**
     * @return mixed
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

    /**
     * @param mixed $citizen
     */
    public function setCitizen($citizen): void
    {
        $this->citizen = $citizen;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeOfPorperty(): ?string
    {
        return $this->typeOfPorperty;
    }

    public function setTypeOfPorperty(?string $typeOfPorperty): self
    {
        $this->typeOfPorperty = $typeOfPorperty;

        return $this;
    }

    public function getArea(): ?string
    {
        return $this->area;
    }

    public function setArea(?string $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }
}
