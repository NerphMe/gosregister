<?php

namespace App\Entity;

use App\Repository\CitizenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CitizenRepository", repositoryClass=CitizenRepository::class)
 */
class Citizen
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gendor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placeOfResidence;

    /**
     * @ORM\OneToMany(targetEntity="Cars",mappedBy="citizen",cascade={"persist"})
     */
    private $cars;

    /**
     * @ORM\OneToMany(targetEntity="Documents", mappedBy="citizen", cascade={"persist"})
     */
    private $passport;

    /**
     * @ORM\OneToMany(targetEntity="TheProperty", mappedBy="citizen")
     */
    private $theProperty;

    /**
     * @ORM\OneToOne(targetEntity="DriverLicense", mappedBy="citizen",fetch="EAGER")
     */
    private $driverLicense;

    /**
     * @ORM\OneToMany(targetEntity="EducationDocs", mappedBy="citizen")
     */
    private $educationDocs;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
        $this->passport = new ArrayCollection();
        $this->educationDocs = new ArrayCollection();
        $this->theProperty = new ArrayCollection();
    }


    public function getCars(): Collection
    {
        return $this->cars;
    }

    /**
     * @param ArrayCollection $cars
     */
    public function setCars(ArrayCollection $cars): void
    {
        $this->cars = $cars;
    }

    public function addCars(Cars $cars): self
    {
        if (!$this->cars->contains($cars)) {
            $this->cars[] = $cars;
            $cars->setCitizen($this);
        }

        return $this;
    }

    public function removeCars(Cars $cars): self
    {
        if ($this->cars->contains($cars)) {
            $this->cars->removeElement($cars);
            // set the owning side to null (unless already changed)
            if ($cars->getCitizen() === $this) {
                $cars->setCitizen(null);
            }
        }

        return $this;
    }

    public function getPassport(): Collection
    {
        return $this->passport;
    }

    /**
     * @param ArrayCollection $passport
     */
    public function setPassport(ArrayCollection $passport): void
    {
        $this->passport = $passport;
    }

    /**
     * @return mixed
     */
    public function getTheProperty()
    {
        return $this->theProperty;
    }

    /**
     * @param mixed $theProperty
     */
    public function setTheProperty($theProperty): void
    {
        $this->theProperty = $theProperty;
    }


    public function getDriverLicense()
    {
        return $this->driverLicense;
    }

    public function setDriverLicense( $driverLicense): void
    {
        $this->driverLicense = $driverLicense;
    }


    public function getEducationDocs(): Collection
    {
        return $this->educationDocs;
    }

    /**
     * @param ArrayCollection $educationDocs
     */
    public function setEducationDocs(ArrayCollection $educationDocs): void
    {
        $this->educationDocs = $educationDocs;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurName()
    {
        return $this->surName;
    }

    /**
     * @param mixed $surName
     */
    public function setSurName($surName): void
    {
        $this->surName = $surName;
    }


    /**
     * @return mixed
     */
    public function getPlaceOfResidence()
    {
        return $this->placeOfResidence;
    }

    /**
     * @param mixed $placeOfResidence
     */
    public function setPlaceOfResidence($placeOfResidence): void
    {
        $this->placeOfResidence = $placeOfResidence;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getGendor(): ?string
    {
        return $this->gendor;
    }

    public function setGendor(string $gendor): self
    {
        $this->gendor = $gendor;

        return $this;
    }


}
