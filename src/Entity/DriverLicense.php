<?php

namespace App\Entity;

use App\Repository\DriverLicenseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DriverLicenseRepository::class)
 */
class DriverLicense
{
    /**
     * @return mixed
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

    /**
     * @param mixed $citizen
     */
    public function setCitizen($citizen): void
    {
        $this->citizen = $citizen;
    }
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $categoryAuto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licenseId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serialNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $issuedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $whenIssued;

    /**
     * @ORM\OneToOne(targetEntity="Citizen", inversedBy="driverLicense")
     */
    private $citizen;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryAuto(): ?string
    {
        return $this->categoryAuto;
    }

    public function setCategoryAuto(?string $categoryAuto): self
    {
        $this->categoryAuto = $categoryAuto;

        return $this;
    }

    public function getLicenseId(): ?string
    {
        return $this->licenseId;
    }

    public function setLicenseId(?string $licenseId): self
    {
        $this->licenseId = $licenseId;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getIssuedBy(): ?string
    {
        return $this->issuedBy;
    }

    public function setIssuedBy(?string $issuedBy): self
    {
        $this->issuedBy = $issuedBy;

        return $this;
    }

    public function getWhenIssued(): ?string
    {
        return $this->whenIssued;
    }

    public function setWhenIssued(?string $whenIssued): self
    {
        $this->whenIssued = $whenIssued;

        return $this;
    }
}
