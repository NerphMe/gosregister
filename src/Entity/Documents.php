<?php

namespace App\Entity;

use App\Repository\DocumentsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentsRepository::class)
 */
class Documents
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $serialNumber;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $passport;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $international_passport;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $issuedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $whenIssued;

    /**
     * @ORM\ManyToOne(targetEntity="Citizen", inversedBy="passport")
     */
    private $citizen;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $untilWhenValid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $SerialNumber): self
    {
        $this->serialNumber = $SerialNumber;

        return $this;
    }

    public function getIssuedBy(): ?string
    {
        return $this->issuedBy;
    }

    public function setIssuedBy(?string $issuedBy): self
    {
        $this->issuedBy = $issuedBy;

        return $this;
    }

    public function getWhenIssued(): ?string
    {
        return $this->whenIssued;
    }

    public function setWhenIssued(string $whenIssued): self
    {
        $this->whenIssued = $whenIssued;

        return $this;
    }

    public function getUntilWhenValid(): ?string
    {
        return $this->untilWhenValid;
    }

    public function setUntilWhenValid(?string $untilWhenValid): self
    {
        $this->untilWhenValid = $untilWhenValid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternationalPassport()
    {
        return $this->international_passport;
    }

    /**
     * @param mixed $international_passport
     */
    public function setInternationalPassport($international_passport): void
    {
        $this->international_passport = $international_passport;
    }

    /**
     * @return mixed
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

    /**
     * @param mixed $citizen
     */
    public function setCitizen($citizen): void
    {
        $this->citizen = $citizen;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $passport
     */
    public function setPassport($passport): void
    {
        $this->passport = $passport;
    }

}
