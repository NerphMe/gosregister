<?php

namespace App\Entity;

use App\Repository\TrafficFinesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrafficFinesRepository::class)
 */
class TrafficFines
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fines;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $finesCost;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $statusOfFines;

    /**
     * @ORM\ManyToOne(targetEntity="Cars", inversedBy="trafficFines")
     */
    private $cars;

    /**
     * @return mixed
     */
    public function getCars()
    {
        return $this->cars;
    }

    /**
     * @param mixed $cars
     */
    public function setCars($cars): void
    {
        $this->cars = $cars;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFines(): ?string
    {
        return $this->fines;
    }

    public function setFines(?string $fines): self
    {
        $this->fines = $fines;

        return $this;
    }

    public function getFinesCost(): ?int
    {
        return $this->finesCost;
    }

    public function setFinesCost(?int $finesCost): self
    {
        $this->finesCost = $finesCost;

        return $this;
    }

    public function getStatusOfFines(): ?bool
    {
        return $this->statusOfFines;
    }

    public function setStatusOfFines(?bool $statusOfFines): self
    {
        $this->statusOfFines = $statusOfFines;

        return $this;
    }
}
